import openai
from Keys.keys import OPEN_AI_KEY
## API Key from signing up On OpenAI Website
## For lack of a better method, I store inside my .venv so make one or replace OPEN_AI_KEY with you api key
## Free 5$ credit (lasts forrrrrevvvver) but with low rate limit
## good for testing.

## built in variable can set to your api key
openai.api_key = OPEN_AI_KEY

## creating the prompt to send in.
prompt = """
List me 5 colors in order of your favorite.
"""

## most basic GPT request. you need a model type
## you need a message that has the role and the content
## role, is user, assisant(gpt itself), and system(seed GPT with a base prompt)
gpt_reponse = openai.ChatCompletion.create(
    model="gpt-3.5-turbo",
    messages=[{"role":"user", "content": prompt}]
)

## returns a dictionary not JSON
## can just pull directly from the response
print(gpt_reponse["choices"][0]["message"]["content"])
