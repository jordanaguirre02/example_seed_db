## this whole script takes like 45 minutes to complete, cocktail_data.txt
## would have all the final data for this example
## cost about 75k tokens or 0.15 cents

import openai
from openai.error import RateLimitError, APIError
import requests
import json
from time import sleep
from Keys.keys import OPEN_AI_KEY


## set api key variable to your api key
openai.api_key = OPEN_AI_KEY

## our database starts at 0
drink_db = {}

## for letter finding
i = 0

## to search all 26 letters
while i < 27:
    ## turn unicode to a letter. 65 = "a"
    letter = chr(65 + i)

    ## Printed for my own sanity
    print(letter)

    ## cocktail db search by letter url and the response from
    ## that request loaded in to out drink_list
    dbURL = f"http://www.thecocktaildb.com/api/json/v1/1/search.php?f={letter}"
    response = requests.get(dbURL)
    data = response.content
    drink_list = json.loads(data)["drinks"]

    ## check to make sure not null
    if drink_list is not None:
        ## loop through each drink in drink_list
        for drink in drink_list:
            ## code just to help with visualization of where in script it is
            if drink == drink_list[0]:
                d_num = 1
            print(f"Creating Drink Number {d_num}")

            ## create a drink dictionary that will be added to our database
            ## ingredient list is for chat gpt
            drink_attribute_list = {}
            ingredient_list = []

            ## more null checks and pull cocktail_db data into our database
            ## grabbing only what I want and naming it to fit our database
            if drink["strDrink"] is not None:
                drink_attribute_list["our_name"] = drink["strDrink"]
            if drink["strAlcoholic"] is not None:
                drink_attribute_list["alcoholic"] = drink["strAlcoholic"]
            if drink["strDrinkThumb"] is not None:
                drink_attribute_list["drink_picture"] = drink["strDrinkThumb"]

            ## loop through all possible ingredients
            for num in range(1,16):
                ## grab ingredient picture url
                ingredient_URL = f"http://www.thecocktaildb.com/images/ingredients/{drink[f'strIngredient{num}']}.png"

                ## more null check creates an ingredient dictionary for each ingredient
                ## that has the ingredient, measurement, and picture url
                if (drink[f"strIngredient{num}"] is not None
                    and
                    drink[f"strMeasure{num}"] is not None):
                        drink_attribute_list[f"ingredient{num}"] = {
                            "ingredient": drink[f"strIngredient{num}"],
                            "measurement": drink[f"strMeasure{num}"],
                            "ingredient_picture": ingredient_URL
                        }
                        ## if not null add just the ingredient to ingredient list
                        ingredient_list.append(drink[f"strIngredient{num}"])

            ## gpt prompt creation
            ## tailoring it to return a drink name and a brief flavor profile
            ## for each drink based on the ingredient list
            prompt = f"""Based on this list of drink ingredients,
                        create a short flavor profile for the '{drink["strDrink"]}' drink.
                        Ingedient list is: {ingredient_list}"""


            ## come error handling, attempt to get gpt response
            ## if hit error from gpt from ratelimit or general gpt error
            ## wait a second and try again until response is gotten
            ## grab flavor profile out of response and print for sanity
            while True:
                try:
                    gpt_response = openai.ChatCompletion.create(
                        model="gpt-3.5-turbo",
                        messages=[
                            {"role": "user", "content": prompt},
                        ],
                        temperature=0.5,
                        max_tokens=100
                    )
                    flavor_profile = gpt_response["choices"][0]["message"]["content"]
                    print(f"\nDrink Flavor Profile: \n{flavor_profile}\n")
                    break
                except (RateLimitError, APIError):
                    sleep(1)

            ## add flavor profile to the current drink's attributes
            drink_attribute_list["flavor_profile"] = flavor_profile

            ## more null checks, make sure new drink has ALL these fields
            if (
                "our_name" in drink_attribute_list.keys() and
                "alcoholic" in drink_attribute_list.keys() and
                "drink_picture" in drink_attribute_list.keys() and
                "ingredient1" in drink_attribute_list.keys() and
                "flavor_profile" in drink_attribute_list.keys()
            ):
                ## add drink to our new database
                drink_db[drink["strDrink"]] = drink_attribute_list

            ## move onto next drink after 0.3s
            d_num += 1
            sleep(0.3)
    ## move onto next letter after 1.5s
    i += 1
    sleep(1.5)


## print size of our new database to let us know script is completed
print()
print(len(drink_db))

## write to text file for view of data for sanity and testing
with open("cocktail_data.txt", "w") as db_file:
    db_file.write(str(drink_db))
    db_file.close()
